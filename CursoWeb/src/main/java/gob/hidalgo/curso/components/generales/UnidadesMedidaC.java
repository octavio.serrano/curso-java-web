package gob.hidalgo.curso.components.generales;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.generales.UnidadMedidaEO;
import gob.hidalgo.curso.utils.Modelo;

@Component("UnidadesMedidaC")
public class UnidadesMedidaC {
	@Autowired
	private SqlSession sqlSession;
	@Autowired
	private MensajesC mensajesC;
	
	public Modelo<UnidadMedidaEO> modelo(){
		List<UnidadMedidaEO> listado=sqlSession.selectList("generales.unidadesMedida.listado");
		return new Modelo<UnidadMedidaEO>(listado);
	}
	
	public UnidadMedidaEO nuevo () {
			return new UnidadMedidaEO();
	}
	
	public boolean guardar (UnidadMedidaEO unidad) {
		if (unidad.getId()==null) {
			sqlSession.insert("generales.unidadesMedida.insertar", unidad);
			mensajesC.mensajeInfo("se inserto correctamente");
		}
		else {
			sqlSession.update("generales.unidadesMedida.actualizar", unidad);
			mensajesC.mensajeInfo("se actualizo correctamente");

		}
			return true;
	}
	
	public boolean eliminar(UnidadMedidaEO unidad) {
		sqlSession.delete("generales.unidadesMedida.eliminar", unidad);
		mensajesC.mensajeInfo("se elimino correctamente");
		return true;
	}
}
