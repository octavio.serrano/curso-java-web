package gob.hidalgo.curso.components.generales;

import java.util.List;


import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.generales.ClienteEO;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("ClientesC")
public class ClientesC {
	
	
	@Autowired
	private SqlSession sqlSession;
	
	@Autowired
	private MensajesC mensajesC;

	public ClientesC() {
		super();
		log.debug("Creacion componente ClientesC");
	}
	
	public Modelo<ClienteEO> modelo() {
        List<ClienteEO> listado;
		listado = sqlSession.selectList("generales.clientes.listado");
		return new Modelo<ClienteEO>(listado);
	}
	
	 public ClienteEO nuevo() {
		 return new ClienteEO();
	 }
	 
	 public boolean guardar(ClienteEO cliente) {
		 
		 if (cliente.getCelular().length() != 10) {
			 mensajesC.mensajeError("Favor de anotar un celular valido");
			 return false;
		 }
		 
		 if (cliente.getId() == null) {
			 sqlSession.insert("generales.clientes.insert", cliente);
			 mensajesC.mensajeInfo("cliente agregado");			 
			 
		 }else {
			 sqlSession.update("generales.clientes.actualizar", cliente);
			 mensajesC.mensajeInfo("cliente actualizado" );
		 }
		return false;
		 
	 }
	 
	 public boolean eliminar(ClienteEO cliente) {
		 sqlSession.delete("generales.cliente.eliminar", cliente);
		 mensajesC.mensajeInfo("cliente eliminado");
		 return true;
	 }

}
