package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("ClienteEO")
public class ClienteEO extends EntityObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
		private Integer id;
		private String curp;
		
		@NotBlank(message = "Insertar nombre")
		private String nombre;
		
		@NotBlank(message = "Insertar primer apellido")
		private String primerApellido;
		
		@NotBlank(message = "Insertar segundo apellido")
		private String segundoApellido;
		
		@NotNull(message = "seleccionar fecha de nacimiento")
		@PastOrPresent(message = "selecciona una fecha valida")
		private LocalDate fechaNacimiento;
		
		@Email(message = "Insertar un mail valido")
		private String mail;
		
		@NotBlank(message = "insertar num celular")
		private String celular;
		
		private List<OrdenPagoEO> ordenesPago;
		
		@JsonIgnore
		private String fechaNacimientoT;
		
		
		public String getFechaNacimientoT() {
			if(fechaNacimientoT == null) {
			   if(fechaNacimiento != null) {
				fechaNacimientoT = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			} else {
				fechaNacimientoT = "";
			}
			}   
			
		 return fechaNacimientoT;	
			
		}
}

