package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import org.apache.ibatis.type.Alias;
import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("UnidadMedidaEO")
public class UnidadMedidaEO extends EntityObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
		private Integer id;
		@NotBlank(message="Favor de anotar le nombre")
		private String nombre;
		private String siglas;

		
	
		
}

